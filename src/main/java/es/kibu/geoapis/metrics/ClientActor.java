package es.kibu.geoapis.metrics;

import akka.actor.Cancellable;
import akka.actor.Props;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import es.kibu.geoapis.api.Configurable;
import es.kibu.geoapis.api.Tracker;
import es.kibu.geoapis.backend.actors.tools.ToolsActor;
import es.kibu.geoapis.data.DataCreator;
import es.kibu.geoapis.data.VariableDataGenerator;
import es.kibu.geoapis.data.providers.RandomUserAndSessionProvider;
import es.kibu.geoapis.data.providers.components.DaggerRandomGeneratorMaker;
import es.kibu.geoapis.data.providers.components.RandomGeneratorMaker;
import es.kibu.geoapis.data.providers.modules.RandomProvidersModule;
import es.kibu.geoapis.metrics.actors.data.consumer.tools.BasicToolsActor;
import es.kibu.geoapis.metrics.api.JavaMetricsApi;
import es.kibu.geoapis.metrics.config.ConfigUtils;
import es.kibu.geoapis.objectmodel.MetricsDefinition;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import scala.concurrent.duration.FiniteDuration;
import sun.plugin2.message.HeartbeatMessage;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.TimeUnit;


/**
 * Created by lrodriguez2002cu on 03/03/2017.
 */
public class ClientActor extends ToolsActor {

    public static final String METRICS_SERVICES_URL = "metrics-services-url";

    Logger logger = LoggerFactory.getLogger(BasicToolsActor.class);

    String applicationId;

    Configurable config;

    ClientActorMessage clientActorMessage;

    private String url;

    public static class ClientActorMessage implements Serializable {

        public Configurable getConfigurable() {
            return configurable;
        }

        public void setConfigurable(Configurable configurable) {
            this.configurable = configurable;
        }

        public MetricsDefinition getMetricsDefinition() {
            return metricsDefinition;
        }

        public void setMetricsDefinition(MetricsDefinition metricsDefinition) {
            this.metricsDefinition = metricsDefinition;
        }

        public String getApplicationId() {
            return applicationId;
        }

        public void setApplicationId(String applicationId) {
            this.applicationId = applicationId;
        }

        Configurable configurable;
        MetricsDefinition metricsDefinition;
        String applicationId;

        public String getVariableName() {
            return variableName;
        }

        public void setVariableName(String variableName) {
            this.variableName = variableName;
        }

        String variableName;
        int numberOfIterations = -1;
        int intervalMs = 1000;
        int startDelayMs = 500;

        public ClientActorMessage(Configurable configurable, MetricsDefinition metricsDefinition, String variableName) {
            /*this.configurable = configurable;
            this.metricsDefinition= metricsDefinition;
            this.variableName = variableName;*/
            this(configurable, metricsDefinition, variableName, -1, 1000, 500);
        }

        public ClientActorMessage(Configurable configurable, MetricsDefinition metricsDefinition, String variableName, int numberOfIterations, int intervalMs, int startDelayMs) {
            this.configurable = configurable;
            this.metricsDefinition = metricsDefinition;
            this.variableName = variableName;
            this.numberOfIterations = numberOfIterations;
            this.intervalMs = intervalMs;
            this.startDelayMs = startDelayMs;
        }


        public ClientActorMessage(String applicationId, String variableName) {
            this.configurable = null;
            this.applicationId = applicationId;
            this.variableName = variableName;
            this.numberOfIterations = -1;
            //this(null, null, applicationId, variableName, -1);
        }


        public int getNumberOfIterations() {
            return numberOfIterations;
        }

        public void setNumberOfIterations(int numberOfIterations) {
            this.numberOfIterations = numberOfIterations;
        }

        public int getIntervalMs() {
            return intervalMs;
        }

        public void setIntervalMs(int intervalMs) {
            this.intervalMs = intervalMs;
        }

        public int getStartDelayMs() {
            return startDelayMs;
        }

        public void setStartDelayMs(int startDelayMs) {
            this.startDelayMs = startDelayMs;
        }
    }

    public ClientActor(String applicationId) {
        super();
        this.applicationId = applicationId;
        configUrl();
    }

    public ClientActor(Configurable config) {
        super();
        this.config = config;
        configUrl();

    }

    private void configUrl() {
        url = ConfigUtils.getConfig().getString(METRICS_SERVICES_URL);
    }

    @Override
    public boolean getDoHeartbeat() {
        return false;
    }

    public static Props props(String applicationId) {
        return Props.create(ClientActor.class, applicationId);
    }

    public static Props props(Configurable config) {
        return Props.create(ClientActor.class, config);
    }

    int numberOfIterations = -1;
    int intervalMs = 1000;
    int startDelayMs = 500;
    int iterationCounter = 0;

    @Override
    public void onReceive(Object message) throws Throwable {
        super.onReceive(message);
        logger.debug("Client '{}' is running", getSelf().path().name());

        if (message instanceof HeartbeatMessage) {
            logger.debug("Heartbeat received at '{}'", getSelf().path().name());
        }

        if (message instanceof ClientActorMessage) {
            ClientActorMessage clientMessage = (ClientActorMessage) message;
            Configurable config = clientMessage.getConfigurable();

            numberOfIterations = clientMessage.numberOfIterations;
            intervalMs = clientMessage.intervalMs;
            startDelayMs = clientMessage.startDelayMs;
            iterationCounter = 0;

            if (!JavaMetricsApi.isInitialized()) {
                logger.debug("Initializing JavaMetricsApi with url: {} \n StartDelay: {} ms \n intervalMs: {}", url,
                        startDelayMs,
                        intervalMs);

                JavaMetricsApi.init(config, url);
            }
            clientActorMessage = clientMessage;

            scheduleSendNext();

        } else {
            if (message instanceof SendDataMessage) {
                iterationCounter ++;
                logger.info("sending data");

                if (numberOfIterations != -1) {
                    logger.info("Iteration number: {}", numberOfIterations);
                    numberOfIterations--;
                    if (numberOfIterations == 0) {
                        tick.cancel();
                    }
                }

                sendData((SendDataMessage) message);

            }
        }

    }

    Cancellable tick;

    @Override
    public void preStart() throws Exception {
        super.preStart();
    }

    @Override
    public void postStop() throws Exception {
        super.postStop();
        tick.cancel();
    }

    static class SendDataMessage implements Serializable {
        Object data;
        String variableName;
        Configurable config;

        public SendDataMessage(Object data, String variableName, Configurable config) {
            this.data = data;
            this.variableName = variableName;
            this.config = config;
        }
    }

    private Map<String, Object> jsonToMap(JsonObject generatedObject) {
        Map<String, Object> map = new HashMap();
        for (Map.Entry<String, JsonElement> entry : generatedObject.entrySet()) {
            JsonElement value = entry.getValue();
            map.put(entry.getKey(), value);
        }
        return map;
    }

    private void sendData(SendDataMessage message) {

        String variableName = message.variableName;
        Object generatedObject = message.data;
        Tracker defaultTracker = JavaMetricsApi.getInstance().getDefaultTracker();
        Map<String, Object> map = jsonToMap((JsonObject) generatedObject);
        logger.debug("Sending data #{} variable:{}, data: {}", iterationCounter,  variableName, map);
        defaultTracker.send(variableName, map);
    }

    DataCreator dataCreator;

    private void scheduleSendNext() {

        logger.info("Creating generator and scheduling..");

        logger.info("Creating data generator..");

        RandomGeneratorMaker randomMaker = DaggerRandomGeneratorMaker.builder()
                .randomProvidersModule(new RandomProvidersModule(new RandomUserAndSessionProvider(numberOfIterations), config))
                .build();

        dataCreator = randomMaker.generator();

        logger.info("Creating setting up the scheduling..");
        tick = getContext().system().scheduler()
                .schedule(new FiniteDuration(startDelayMs, TimeUnit.MILLISECONDS), new FiniteDuration(intervalMs, TimeUnit.MILLISECONDS), new Runnable() {
                    @Override
                    public void run() {
                        logger.info("Sending new message to client actor ..");
                        SendDataMessage message =
                                new SendDataMessage(
                                        getGeneratedObject(clientActorMessage),
                                        clientActorMessage.getVariableName(),
                                        clientActorMessage.getConfigurable());

                        getSelf().tell(message, getSelf());

                        ClientActor.this.getSender();
                    }
                }, getContext().system().dispatcher());
        logger.info("Done scheduling.");
    }

    MetricsDefinition metricsDefinition = null;

    private Object getGeneratedObject(ClientActorMessage clientMessage) {

        if (metricsDefinition == null) {
            MetricsDefinition metricsDefinition = clientMessage.getMetricsDefinition() == null ?
                    ((JavaMetricsApi) JavaMetricsApi.getInstance()).getMetricsDefinition() : clientMessage.getMetricsDefinition();
            this.metricsDefinition = metricsDefinition;
        }

        ((VariableDataGenerator) dataCreator).setVariable(metricsDefinition.getVariableByName(clientMessage.getVariableName()));

        return dataCreator.generateObject();
    }


}
