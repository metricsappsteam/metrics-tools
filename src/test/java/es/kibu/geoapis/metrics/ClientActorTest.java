package es.kibu.geoapis.metrics;

import akka.actor.ActorRef;
import akka.actor.ActorSystem;
import akka.actor.Props;
import akka.testkit.JavaTestKit;
import com.typesafe.config.Config;
import es.kibu.geoapis.api.Configurable;
import es.kibu.geoapis.metrics.config.ConfigUtils;
import kamon.Kamon;
import org.junit.BeforeClass;
import org.junit.Test;
import scala.concurrent.duration.FiniteDuration;

import java.util.concurrent.TimeUnit;

/**
 * Created by lrodr_000 on 06/03/2017.
 */
public class ClientActorTest {

    static ActorSystem system;
    static Config config;

    @BeforeClass
    public static void setup() {
        Kamon.start();
        system = ActorSystem.create("ClientActorTestSystem");

        config = ConfigUtils.getConfig();

    }

    @Test
    public void test() throws Exception {

        boolean local = config.getString("metrics-services-url").contains("localhost");
        //in cluster
        final String applicationId = (!local) ? "application-56551122612954585945655112261295458594"
                : "application-46169671277633484794616967127763348479";

        int numberOfIterations = 100;

        new JavaTestKit(system) {{

            Configurable configurable = new Configurable() {
                @Override
                public String getUser() {
                    return null;
                }

                @Override
                public String getApplication() {
                    return applicationId;
                }

                @Override
                public String getSession() {
                    return null;
                }
            };

            final Props clientProps = ClientActor.props(configurable);

            final ActorRef clientRef = system.actorOf(clientProps, "clientActor2");

            //final ActorRef scriptBackend = system.actorOf(props);

            ClientActor.ClientActorMessage message = new ClientActor.ClientActorMessage(configurable,
                    MetricsCoreUtils.withMetricDefinition("sample-documents/sample-metric-wikiloc-box-speed-geostats.json"),
                    "movement", numberOfIterations, 500, 200);


            FiniteDuration duration = FiniteDuration.apply(600, TimeUnit.SECONDS);

            new Within(duration/*duration("10 seconds")*/) {
                protected void run() {

                    clientRef.tell(message, getRef());

                    new Within(duration) {
                        @Override
                        protected void run() {

                            new AwaitCond(duration) {
                                protected boolean cond() {
                                    return msgAvailable();
                                }
                            };
/*
                            clientRef.tell("start", getRef());


                            MetricsMessage newMessage = createMessageSimple("simpleMetricScopes");
                            scriptBackend.tell(newMessage, getRef());
                            MetricsResultMessage resultMessage = expectMsgClass(MetricsResultMessage.class);

                            resultMessage.getRequestMessage().equals(newMessage);
                            assertTrue(resultMessage.getResult().getAsLong().equals(500L));
*/

                        }
                    };
                    /*// response must have been enqueued to us before metricsEngineActor
                    expectMsgEquals(Duration.Zero(), "world");
                    // check that the metricsEngineActor we injected earlier got the msg
                    metricsEngineActor.expectMsgEquals(Duration.Zero(), "hello");
                    Assert.assertEquals(getRef(), metricsEngineActor.getLastSender());
*/
                    // Will wait for the rest of the 3 seconds
                    //expectNoMsg();
                }
            };
        }};
    }
}