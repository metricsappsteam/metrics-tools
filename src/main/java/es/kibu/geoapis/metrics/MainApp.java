package es.kibu.geoapis.metrics;

import akka.actor.ActorSystem;
import com.typesafe.config.Config;
import es.kibu.geoapis.metrics.actors.data.consumer.tools.BasicToolsActor;
import es.kibu.geoapis.metrics.config.ConfigUtils;
import kamon.Kamon;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Created by lrodr_000 on 03/03/2017.
 */
public class MainApp {


    static Logger logger = LoggerFactory.getLogger(ConsumerApp.class);

    public static void main(String[] args) {
        Config config = ConfigUtils.getConfig();

        logger.debug("/-------------------------------------------");
        logger.debug(String.format("| Starting Metrics Tools .."));
        logger.debug("\\------------------------------------------");

        logger.debug(String.format("Starting Kamon.."));
        Kamon.start();
        logger.debug(String.format("Kamon started"));

        ActorSystem system = ActorSystem.create("MetricsToolsSystem", config);

        ConfigUtils.prettyPrintConfig();

        system.actorOf(BasicToolsActor.props());


/*
        KafkaConsumer consumer = new KafkaConsumer();
        consumer.start(system, ConfigUtils.DATA_INGESTION_CONF, SinkFactory.cassandraSinkActor(system));
*/
    }
}
