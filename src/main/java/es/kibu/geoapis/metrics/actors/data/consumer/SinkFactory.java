package es.kibu.geoapis.metrics.actors.data.consumer;

import akka.NotUsed;
import akka.actor.ActorRef;
import akka.actor.ActorSystem;
import akka.stream.IOResult;
import akka.stream.javadsl.FileIO;
import akka.stream.javadsl.Flow;
import akka.stream.javadsl.Keep;
import akka.stream.javadsl.Sink;
import akka.util.ByteString;
import es.kibu.geoapis.metrics.actors.data.WriteInstruction;

import java.nio.file.Paths;
import java.util.concurrent.CompletionStage;

/**
 * Created by lrodr_000 on 30/12/2016.
 */
public class SinkFactory {

    public static final String FILE_SINK = "FILE_SINK";
    public static final String IGNORE_SINK = "IGNORE_SINK";
    public static final String CASSANDRA_SINK = "CASSANDRA_SINK";
    public static final String ACTOR_SINK = "ACTOR_SINK";


    public static Sink<WriteInstruction, ?> getSink(String sinkName, Object conf){

        if (sinkName.equalsIgnoreCase(FILE_SINK)) {
            return toFileSink();
        }
        else if (sinkName.equalsIgnoreCase(CASSANDRA_SINK)) {
            return cassandraSinkActor((ActorSystem) conf);
        }
        else if (sinkName.equalsIgnoreCase(IGNORE_SINK)) {
            return toIgnoreSink();
        }
        else if (sinkName.equalsIgnoreCase(ACTOR_SINK)) {
            return actorSink((ActorRef) conf);
        }
        return  toIgnoreSink();
    }


    public static Sink<WriteInstruction, CompletionStage<IOResult>> toFileSink() {
        return Flow.of(WriteInstruction.class)
                .map(s -> ByteString.fromString(s.toString() + "\n"))
                .toMat(FileIO.toPath(Paths.get("test.kafka.text")), Keep.right());
    }

    public static Sink<WriteInstruction, NotUsed> toIgnoreSink() {
        return Flow.of(WriteInstruction.class)
                .map(s -> ByteString.fromString(s.toString() + "\n"))
                .toMat(Sink.ignore(), Keep.none());
    }

    public static Sink<WriteInstruction, NotUsed> cassandraSinkActor(ActorSystem system) {
        ActorRef actorRef = system.actorOf(ConsumerForwarderActor.props());
        return Flow.of(WriteInstruction.class)
                .toMat(Sink.actorRef(actorRef, new ConsumerForwarderActor.CompleteObject()), Keep.none());
    }

    public static Sink<WriteInstruction, NotUsed> actorSink(ActorRef actorRef) {
        return Flow.of(WriteInstruction.class)
                .toMat(Sink.actorRef(actorRef, new ConsumerForwarderActor.CompleteObject()), Keep.none());
    }


}
