package es.kibu.geoapis.metrics.actors.data.consumer;

import akka.Done;
import akka.NotUsed;
import akka.actor.*;
import akka.http.javadsl.Http;
import akka.http.javadsl.model.*;
import akka.japi.pf.ReceiveBuilder;
import akka.stream.*;
import akka.stream.javadsl.*;
import akka.testkit.JavaTestKit;
import akka.util.ByteString;
import com.github.fge.jsonschema.core.exceptions.ProcessingException;
import com.typesafe.config.Config;
import com.typesafe.config.ConfigFactory;
import es.kibu.geoapis.metrics.actors.data.WriteInstruction;
import es.kibu.geoapis.metrics.config.ConfigUtils;
import mockit.Expectations;
import mockit.Verifications;
import mockit.integration.junit4.JMockit;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import scala.concurrent.duration.Duration;
import scala.concurrent.duration.FiniteDuration;
import scala.reflect.ClassTag;

import java.io.IOException;
import java.io.InputStream;
import java.io.Serializable;
import java.math.BigInteger;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.concurrent.CompletionStage;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeUnit;


/**
 * Created by lrodriguez2002cu on 21/12/2016.
 */

@RunWith(JMockit.class)
public class KafkaConsumerTest {

    public static final String SAMPLE_METRICS_SAMPLE_METRIC_WIKILOC_BOX_SPEED_JSON = "sample-metrics/sample-metric-wikiloc-box-speed.json";

    public static final String RESTAPI_ENDPOINT = "http://%s:8082/topics/metrics-data";
    public static final String AKKA_KAFKA_CONSUMER_BOOTSTRAP_SERVERS = "akka.kafka.consumer.bootstrap.servers";

    public static final String getRestEndPoint(){
        if (ConfigUtils.getConfig().hasPath(AKKA_KAFKA_CONSUMER_BOOTSTRAP_SERVERS)) {
            String server = ConfigUtils.getConfig().getString(AKKA_KAFKA_CONSUMER_BOOTSTRAP_SERVERS).split(":")[0];
            return  String.format(RESTAPI_ENDPOINT, server);
        }
        throw new RuntimeException("Endpoint server not defined");
    }

    @Test
    @Ignore
    public void testStream() {

        final ActorSystem system = ActorSystem.create("KafkaConsumerTest");
        final Materializer materializer = ActorMaterializer.create(system);

        final Source<Integer, NotUsed> source = Source.range(1, 100);

        final Source<BigInteger, NotUsed> factorials =
                source
                        .scan(BigInteger.ONE, (acc, next) -> acc.multiply(BigInteger.valueOf(next)));

        final CompletionStage<IOResult> result =
                factorials
                        .map(num -> ByteString.fromString(num.toString() + "\n"))
                        .runWith(FileIO.toPath(Paths.get("factorials.txt")), materializer);


        factorials.map(BigInteger::toString).runWith(lineSink("factorial2.txt"), materializer);
    }


    public Sink<String, CompletionStage<IOResult>> lineSink(String filename) {
        return Flow.of(String.class)
                .map(s -> ByteString.fromString(s.toString() + "\n"))
                .toMat(FileIO.toPath(Paths.get(filename)), Keep.right());
    }


    public Sink<Author, CompletionStage<IOResult>> authorsSink(String filename) {
        return Flow.of(Author.class)
                .map(s -> ByteString.fromString(s.toString() + "\n"))
                .toMat(FileIO.toPath(Paths.get(filename)), Keep.right());
    }


    public Sink<Hashtag, CompletionStage<IOResult>> hashtagsSink(String filename) {
        return Flow.of(Hashtag.class)
                .map(s -> ByteString.fromString(s.toString() + "\n"))
                .toMat(FileIO.toPath(Paths.get(filename)), Keep.right());
    }

    class Tweet {
        Author author;
        List<Hashtag> hashtags;

        public List<Hashtag> hashtags() {
            return this.hashtags;
        }

        public Tweet(Author author, List<Hashtag> hashtags) {
            this.author = author;
            this.hashtags = hashtags;
        }
    }

    class Author {
        String name;

        public Author(String name) {
            this.name = name;
        }

        @Override
        public String toString() {
            return "Author{" +
                    "name='" + name + '\'' +
                    '}';
        }
    }

    class Hashtag {
        String id;

        public Hashtag(String id) {
            this.id = id;
        }

        @Override
        public String toString() {
            return "Hashtag{" +
                    "id='" + id + '\'' +
                    '}';
        }
    }

    private List<Tweet> createTweets() {
        List<Hashtag> hashtags = new ArrayList<>();
        hashtags.add(new Hashtag("life"));
        hashtags.add(new Hashtag("sunday"));
        hashtags.add(new Hashtag("monday"));

        List<Hashtag> hashtags1 = new ArrayList<>();
        hashtags1.add(new Hashtag("life1"));
        hashtags1.add(new Hashtag("sunday1"));
        hashtags1.add(new Hashtag("monday1"));


        List<Tweet> tweets = new ArrayList<>();
        tweets.add(new Tweet(new Author("luis"), hashtags));
        tweets.add(new Tweet(new Author("luis1"), hashtags1));

        return tweets;
    }

    @Test
    @Ignore
    public void testGraph() {


        final ActorSystem system = ActorSystem.create("QuickStart");
        final Materializer mat = ActorMaterializer.create(system);

        List<Tweet> tweetList = createTweets();
        Source<Tweet, NotUsed> tweets = Source.from(tweetList);

        final Sink<Author, CompletionStage<IOResult>> writeAuthors = authorsSink("authors.txt");
        final Sink<Hashtag, CompletionStage<IOResult>> writeHashtags = hashtagsSink("hashtags.txt");

        Graph<ClosedShape, NotUsed> graph = GraphDSL.create(b -> {

            final UniformFanOutShape<Tweet, Tweet> bcast = b.add(Broadcast.create(2));

            final FlowShape<Tweet, Author> toAuthor =
                    b.add(Flow.of(Tweet.class).map(t -> t.author));

            final FlowShape<Tweet, Hashtag> toTags =
                    b.add(Flow.of(Tweet.class).mapConcat(t -> new ArrayList<>(t.hashtags())));

            final SinkShape<Author> authors = b.add(writeAuthors);
            final SinkShape<Hashtag> hashtags = b.add(writeHashtags);

            b.from(b.add(tweets)).viaFanOut(bcast).via(toAuthor).to(authors);
            b.from(bcast).via(toTags).to(hashtags);

            return ClosedShape.getInstance();
        });


        RunnableGraph<NotUsed> notUsedRunnableGraph = RunnableGraph.fromGraph(graph);

        notUsedRunnableGraph.run(mat);


        system.awaitTermination(new FiniteDuration(30, TimeUnit.SECONDS));
    }


    @Test
    @Ignore
    public void testStream2() throws ExecutionException, InterruptedException {


        final ActorSystem system = ActorSystem.create("QuickStart");
        final Materializer materializer = ActorMaterializer.create(system);

        final Source<Integer, NotUsed> source = Source.range(1, 100);

        final Source<BigInteger, NotUsed> factorials =

                source.scan(BigInteger.ONE, (acc, next) -> acc.multiply(BigInteger.valueOf(next)));


        final CompletionStage<Done> done =
                factorials
                        .zipWith(Source.range(0, 99), (num, idx) -> String.format("%d! = %s", idx, num))
                        .throttle(1, Duration.create(1, TimeUnit.SECONDS), 1, ThrottleMode.shaping())
                        .runForeach(s -> System.out.println(s), materializer);

        done.toCompletableFuture().get();
    }

    @Test
    public void test() throws ExecutionException, InterruptedException {
        Config config = ConfigFactory.load();
        ActorSystem system = ActorSystem.create("kafkaSystem", config);
        ActorMaterializer mat = ActorMaterializer.create(system);

        final Source<Integer, NotUsed> in = Source.from(Arrays.asList(1, 2, 3, 4, 5));

        final Sink<List<String>, CompletionStage<List<String>>> sink = Sink.head();
        final Flow<Integer, Integer, NotUsed> f1 = Flow.of(Integer.class).map(elem -> elem + 10);
        final Flow<Integer, Integer, NotUsed> f2 = Flow.of(Integer.class).map(elem -> elem + 20);
        final Flow<Integer, String, NotUsed> f3 = Flow.of(Integer.class).map(elem -> elem.toString());
        final Flow<Integer, Integer, NotUsed> f4 = Flow.of(Integer.class).map(elem -> elem + 30);

        Graph<ClosedShape, CompletionStage<List<String>>> graph = GraphDSL     // create() function binds sink, out which is sink's out port and builder DSL
                .create(   // we need to reference out's shape in the builder DSL below (in to() function)
                        sink,                // previously created sink (Sink)
                        (builder, out) -> {  // variables: builder (GraphDSL.Builder) and out (SinkShape)
                            final UniformFanOutShape<Integer, Integer> bcast = builder.add(Broadcast.create(2));
                            final UniformFanInShape<Integer, Integer> merge = builder.add(Merge.create(2));

                            final Outlet<Integer> source = builder.add(in).out();
                            builder.from(source).via(builder.add(f1))
                                    .viaFanOut(bcast).via(builder.add(f2)).viaFanIn(merge)
                                    .via(builder.add(f3.grouped(1000))).to(out);  // to() expects a SinkShape
                            builder.from(bcast).via(builder.add(f4)).toFanIn(merge);
                            return ClosedShape.getInstance();
                        });

        final RunnableGraph<CompletionStage<List<String>>> result =
                RunnableGraph.fromGraph(
                        graph);

        List<String> strings = result.run(mat).toCompletableFuture().get();
        System.out.println(strings);
    }


    /*
    *  This request
    POST /topics/metrics-data HTTP/1.1
     Host: localhost:8082
     Content-Type: application/vnd.kafka.json.v1+json
     Cache-Control: no-cache
     Postman-Token: 71e519f4-df5a-d250-8b02-02d633bb6e0a

     {"records":[
         {
         "key": "app12345:movement2" ,
         "value": {
             "application": "app1",
             "user": "user1",
             "session": "session1"
             }
         }
     ]
    }
    * */
    @Test
    @Ignore
    public void testConsumer() throws ExecutionException, InterruptedException {

        Config config = ConfigFactory.load();
        ActorSystem system = ActorSystem.create("kafkaSystem", config);

        KafkaConsumer consumer = new KafkaConsumer();
        consumer.start(system, "metrics-tools.conf", SinkFactory.cassandraSinkActor(system));

        system.awaitTermination(new FiniteDuration(1, TimeUnit.MINUTES));
    }


    Logger logger = LoggerFactory.getLogger(KafkaConsumerTest.class);
    private static ActorSystem system;

    @BeforeClass
    public static void setUp() throws Exception {
        system = ActorSystem.create();
    }

/*
    class TestHandler {
        void handle(Object message) {

        }
    }

    private static class TestActor extends UntypedActor {

        TestHandler handler;


        public static Props props() {
            ClassTag<TestActor> tag = scala.reflect.ClassTag$.MODULE$.apply(TestActor.class);
            return Props.apply(tag);
        }

        @Override
        public void onReceive(Object message) throws Throwable {
            handler.handle(message);
        }
    }*/

    public static class PostProducer extends AbstractActor {

        Logger logger = LoggerFactory.getLogger(PostProducer.class);

        final Http http = Http.get(context().system());
        //final ExecutionContextExecutor dispatcher = context().dispatcher();
        final Materializer materializer = ActorMaterializer.create(context());

        public static Props props() {
            ClassTag<PostProducer> tag = scala.reflect.ClassTag$.MODULE$.apply(PostProducer.class);
            return Props.apply(tag);
        }


        static class ProduceMessage implements Serializable {
            String url;
            String entity;

            //String header;
            public ProduceMessage(String url, String entity) {
                this.url = url;
                this.entity = entity;
            }
        }

        public PostProducer() {

            receive(ReceiveBuilder
                    .match(ProduceMessage.class, msg -> {
                        fetch(msg).whenComplete((response, ex) -> {
                            logger.debug("Completed {}", response.status());
                        });
                    }).match(Object.class, msg1 -> {
                        logger.debug("Message of class {} logged", msg1);
                    }).build());
        }

        CompletionStage<HttpResponse> fetch(ProduceMessage msg) {
            MediaType.Binary mediaType = MediaTypes.applicationBinary("vnd.kafka.json.v1+json", true, "json");
            ContentType binary = ContentTypes.create(mediaType);
            HttpEntity.Strict entity = HttpEntities.create(binary, ByteString.fromString(msg.entity));
            HttpRequest post = HttpRequest.POST(msg.url).withEntity(entity);
            logger.debug("About to execute request {}", post);
            return http.singleRequest(post, materializer).toCompletableFuture();
        }
    }

    public static final String testBodyTemplate =
            "{\"records\":[\n" +
                    "\t{\n" +
                    "\t\"key\": \"%s\" ,\n" +
                    "\t\"value\": {\n" +
                    "\t\t\"application\": \"%s\",\n" +
                    "\t\t\"user\": \"%s\",\n" +
                    "\t\t\"session\": \"%s\"\n" +
                    "\t\t}\n" +
                    "\t}\n" +
                    "]\n" +
                    "\t\n" +
                    "}";

    public PostProducer.ProduceMessage getSample(String application, String variable, String usr, String session) {
        String body = String.format(testBodyTemplate, application + ":" + variable, application, usr, session);
        logger.debug("Rendered body: {}", body);
        return new PostProducer.ProduceMessage(RESTAPI_ENDPOINT, body);
    }

    public PostProducer.ProduceMessage getSample(String application, String variable) {
        return getSample(application, variable, "usr1", "session1");
    }


    private String getRandomVariable() {
        String variable = "variable";
        return getRandomString(variable);
    }

    private String getRandomApplication() {
        String variable = "application";
        return getRandomString(variable);
    }

    private String getRandomString(String prefix) {
        return prefix + org.joda.time.DateTime.now().getMillis();
    }

    @Test
    public void testConsumerActors(/*@Mocked TestHandler handler*/) throws IOException, ProcessingException {

        InputStream resourceAsStream = KafkaConsumerTest.class.getClassLoader().getResourceAsStream("sample-metrics/sample-metric-wikiloc-box-speed.json");
        //String metricString = IOUtils.toString(resourceAsStream, "UTF-8");
        // final MetricsDefinition metricsDefinition = MetricUtils.withMetricDefinition("sample-metrics/sample-metric-wikiloc-box-speed.json");

        new Expectations() {{
            //this should be called twice
/*
            manager.getMetricsDef((String) any);  times = 2;
            returns(Optional.of(new MetricsStorageUtils.MetricsDef(metricString, "samplehash", "sampleMetricId", true)),
                    Optional.of(new MetricsStorageUtils.MetricsDef(metricString, "samplehash", "sampleMetricId", true)));
*/
        }};

        new JavaTestKit(system) {{

            KafkaConsumer consumer = new KafkaConsumer();
            ActorRef actorRef = getTestActor();
            ActorRef producerActorRef = system.actorOf(PostProducer.props());

            consumer.start(system, "metrics-tools.conf", SinkFactory.actorSink(actorRef));

            // can also use JavaTestKit “from the outside”
            final JavaTestKit probe = new JavaTestKit(system);

            // the run() method needs to finish within 3 seconds
            //MetricsDefinitionCacheActor.MetricsDefinitionActorMessage msg = new MetricsDefinitionCacheActor.MetricsDefinitionActorMessage(appid, metricid, MetricsDefinitionCacheActor.Action.GET);

            final FiniteDuration duration = duration("10 seconds");

            String variable = getRandomVariable();
            String application = getRandomApplication();


            new Within(duration) {
                protected void run() {

                    logger.debug("Expected and variable application: {}, variable: {}", application, variable);
                    producerActorRef.tell(getSample(application, variable), getRef());

                    while (true) {
                        WriteInstruction writeInstruction = expectMsgClass(WriteInstruction.class);

                        logger.debug("Within write instruction: {}", writeInstruction);

                        if (writeInstruction.getVariable().equalsIgnoreCase(variable) &&
                                writeInstruction.getContext().getApplication().equalsIgnoreCase(application)) {
                            getTestActor().tell("ok", ActorRef.noSender());
                            break;
                        }
                    }
                }
            };


            String okMsg = expectMsgClass(String.class);

            //Assertting the ok
            Assert.assertTrue(okMsg.equalsIgnoreCase("ok"));

        }};

        new Verifications() {{

        }};
    }

/*

public void testCacheActorInvalidationWorks() throws IOException, ProcessingException {

        InputStream resourceAsStream = MetricsDefinitionCacheActorTest.class.getClassLoader().getResourceAsStream("sample-metrics/sample-metric-wikiloc-box-speed.json");
        String metricString = IOUtils.toString(resourceAsStream, "UTF-8");
        // final MetricsDefinition metricsDefinition = MetricUtils.withMetricDefinition("sample-metrics/sample-metric-wikiloc-box-speed.json");

        new Expectations() {{

            //this should be called twice
            manager.getMetricsDef((String) any);  times = 2;
            returns(Optional.of(new MetricsStorageUtils.MetricsDef(metricString, "samplehash", "sampleMetricId", true)),
                    Optional.of(new MetricsStorageUtils.MetricsDef(metricString, "samplehash", "sampleMetricId", true)));
        }};

        final String appid = "mocked-app";
        final String metricid = "mocked-metricid";

        new JavaTestKit(system) {{

            final Props props = MetricsDefinitionCacheActor.props();
            final ActorRef subject = system.actorOf(props);

            // can also use JavaTestKit “from the outside”
            final JavaTestKit probe = new JavaTestKit(system);

            // the run() method needs to finish within 3 seconds

            MetricsDefinitionCacheActor.MetricsDefinitionActorMessage msg = new MetricsDefinitionCacheActor.MetricsDefinitionActorMessage(appid, metricid, MetricsDefinitionCacheActor.Action.GET);

            final FiniteDuration duration = duration("40 seconds");

            new Within(duration) {
                protected void run() {
                    subject.tell(msg, getRef());
                    // Will wait for the rest of the 3 seconds
                    MetricsDefinitionCacheActor.MetricsDefinitionActorReplyMessage replyMessage = expectMsgClass(MetricsDefinitionCacheActor.MetricsDefinitionActorReplyMessage.class);

                    Assert.assertTrue(replyMessage.application == appid);
                    Assert.assertTrue(replyMessage.metricRef == metricid);
                }
            };

            //this new message does not cause the retrieval from store.
            new Within(duration) {
                protected void run() {
                    subject.tell(msg, getRef());
                    // Will wait for the rest of the 3 seconds
                    MetricsDefinitionCacheActor.MetricsDefinitionActorReplyMessage replyMessage = expectMsgClass(MetricsDefinitionCacheActor.MetricsDefinitionActorReplyMessage.class);

                    Assert.assertTrue(replyMessage.application == appid);
                    Assert.assertTrue(replyMessage.metricRef == metricid);
                }
            };



            String unknown_appid = appid;//"unknown_appid";
            String unknown_metricid = metricid;//"unknown_metricid";

            //send an invalidate message
            final MetricsDefinitionCacheActor.MetricsDefinitionActorMessage msgInvalidate =
                    MetricsDefinitionCacheActor.MetricsDefinitionActorMessage.createInvalidateMessage(unknown_appid, unknown_metricid);
            subject.tell(msgInvalidate, getRef());

            //lets request it again!!. This should cause the retrieval of the deinition from storage again
            final MetricsDefinitionCacheActor.MetricsDefinitionActorMessage msg1 = new MetricsDefinitionCacheActor.MetricsDefinitionActorMessage(unknown_appid, unknown_metricid, MetricsDefinitionCacheActor.Action.GET);

            new Within(duration) {
                protected void run() {
                    subject.tell(msg1, getRef());
                    // Will wait for the rest of the 3 seconds
                    MetricsDefinitionCacheActor.MetricsDefinitionActorReplyMessage replyMessage = expectMsgClass(MetricsDefinitionCacheActor.MetricsDefinitionActorReplyMessage.class);

                    Assert.assertTrue(replyMessage.application == unknown_appid);
                    Assert.assertTrue(replyMessage.metricRef == unknown_metricid);

                    logger.debug("Replied: {}", replyMessage);
                    Assert.assertTrue(!replyMessage.isError());

                }
            };

        }};

        new Verifications() {{

        }};
    }

*/
}