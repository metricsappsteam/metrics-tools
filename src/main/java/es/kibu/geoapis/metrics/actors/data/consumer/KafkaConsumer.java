package es.kibu.geoapis.metrics.actors.data.consumer;

import akka.NotUsed;
import akka.actor.ActorRef;
import akka.actor.ActorSystem;
import akka.japi.function.Function;
import akka.kafka.ConsumerSettings;
import akka.kafka.KafkaConsumerActor;
import akka.kafka.Subscriptions;
import akka.kafka.javadsl.Consumer;
import akka.stream.*;
import akka.stream.javadsl.FileIO;
import akka.stream.javadsl.Flow;
import akka.stream.javadsl.Keep;
import akka.stream.javadsl.Sink;
import akka.util.ByteString;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.typesafe.config.Config;
import com.typesafe.config.ConfigFactory;
import es.kibu.geoapis.metrics.actors.data.WriteInstruction;
import es.kibu.geoapis.objectmodel.dimensions.Constants;
import org.apache.kafka.clients.consumer.ConsumerConfig;
import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.apache.kafka.common.TopicPartition;
import org.apache.kafka.common.serialization.StringDeserializer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.io.Serializable;
import java.nio.file.Paths;
import java.util.concurrent.CompletionStage;

/**
 * Created by lrodriguez2002cu on 21/12/2016.
 */

public class KafkaConsumer {

    static Logger logger = LoggerFactory.getLogger(KafkaConsumer.class);

    public KafkaConsumer() {

    }

/*   # Produce a message using JSON with the value '{ "foo": "bar" }' to the topic test
    $ curl -X POST -H "Content-Type: application/vnd.kafka.json.v1+json" \
            --data '{"records":[{"value":{"foo":"bar"}}]}' "http://localhost:8082/topics/jsontest"
    {"offsets":[{"partition":0,"offset":0,"error_code":null,"error":null}],"key_schema_id":null,"value_schema_id":null}

    # Create a consumer for JSON data, starting at the beginning of the topic's
    # log. Then consume some data from a topic using the base URL in the first response.

    # Finally, close the consumer with a DELETE to make it leave the group and clean up
    # its resources.
    $ curl -X POST -H "Content-Type: application/vnd.kafka.v1+json" \
            --data '{"name": "my_consumer_instance", "format": "json", "auto.offset.reset": "smallest"}' \
    http://localhost:8082/consumers/my_json_consumer
    {"instance_id":"my_consumer_instance",
            "base_uri":"http://localhost:8082/consumers/my_json_consumer/instances/my_consumer_instance"}
    $ curl -X GET -H "Accept: application/vnd.kafka.json.v1+json" \
    http://localhost:8082/consumers/my_json_consumer/instances/my_consumer_instance/topics/jsontest
            [{"key":null,"value":{"foo":"bar"},"partition":0,"offset":0}]
    $ curl -X DELETE \
    http://localhost:8082/consumers/my_json_consumer/instances/my_consumer_instance

    */

    public void start(ActorSystem system, String configResourceBaseName, Sink<WriteInstruction, NotUsed> sink) {

        final ConsumerSettings<String, String> consumerSettings = getConsumerSettings(system, configResourceBaseName);


        final Function<Throwable, Supervision.Directive> decider = exc -> {
            //if (exc instanceof ArithmeticException)
            return Supervision.resume();
                /*else
                    return Supervision.stop();*/
        };

        //Function<Throwable, Supervision.Directive> decider1 = decider;
        final Materializer materializer = ActorMaterializer.create(ActorMaterializerSettings.create(system).withSupervisionStrategy(decider), system);

        //Consumer is represented by actor
        ActorRef consumer = system.actorOf((KafkaConsumerActor.props(consumerSettings)));

        logger.debug("Creating the sink ...");

        final Sink<WriteInstruction, NotUsed> writeVariableSink = sink/*cassandraSinkActor(system)*/;

        //Manually assign topic partition to it
        logger.debug("Creating consumer ...");

        Consumer
                .plainExternalSource(consumer, Subscriptions.assignment(new TopicPartition("metrics-data", 0)/**/))
                .map(t -> {
                    logger.debug("Mapping ...");
                    String key = (String) t.key();
                    KeyDetails keyDetails = KafkaConsumer.decodeKey(key);
                    String application = keyDetails.application;
                    String variable = keyDetails.variable;

                    logger.debug("getting variable from {} record key:{} value:{}", t.topic(), key, t.value());
                    logger.debug("Variables app:{} variable:{}", application, variable);

                    WriteInstruction writeInstruction = new WriteInstruction(variable, t.value().toString(), getWritingContext(getData(t), variable, "")
                            /*new WriteInstruction.WriteContext("fail","fail","fail","fail","fail" )*/
                    );

                    logger.debug("Instruction: {}", writeInstruction);
                    return writeInstruction;
                })
                .runWith(writeVariableSink, materializer);

    }

    private ConsumerSettings<String, String> getConsumerSettings(ActorSystem system, String configResourceBaseName) {

        String bootstrapServers = "localhost:29092";
        Config config = ConfigFactory.load(configResourceBaseName).getConfig("akka.kafka.consumer");
        bootstrapServers = config.getString("bootstrap.servers");
        if (configResourceBaseName != null && !configResourceBaseName.isEmpty()) {
            logger.debug("Creating consumer settings from: {} ", configResourceBaseName);
            return ConsumerSettings.create(config/*system*/, new StringDeserializer(), new StringDeserializer())
                    .withBootstrapServers(bootstrapServers)
                    //.withGroupId("group1")
                    .withProperty(ConsumerConfig.AUTO_OFFSET_RESET_CONFIG, "earliest");
        } else {
            logger.debug("Creating consumer settings from ActorSystem");
            return ConsumerSettings.create(system, new StringDeserializer(), new StringDeserializer())
                    .withBootstrapServers(bootstrapServers)
                    //.withGroupId("group1")
                    .withProperty(ConsumerConfig.AUTO_OFFSET_RESET_CONFIG, "earliest");
        }
    }

    private WriteInstruction.WriteContext getWritingContext(String data, String variable, String metricId) {
        try {
            ObjectMapper mapper = new ObjectMapper();
            logger.debug("data: {}", data);
            JsonNode jsonNode = mapper.readTree(data);
            if (jsonNode.has(Constants.APPLICATION) && jsonNode.has(Constants.USER) && jsonNode.has(Constants.SESSION)) {
                String application = jsonNode.get(Constants.APPLICATION).asText();
                String user = jsonNode.get(Constants.USER).asText();
                String session = jsonNode.get(Constants.SESSION).asText();
                return new WriteInstruction.WriteContext(application, user, session, variable, metricId);
            }
        } catch (IOException e) {

        }
        return new WriteInstruction.WriteContext("fail", "fail", "fail", "fail", "fail");
    }


    private String getData(ConsumerRecord t) {
        return t.value().toString();
    }


    public static class KeyDetails implements Serializable {
        String variable;
        String application;

        public KeyDetails(String variable, String application) {
            this.variable = variable;
            this.application = application;
        }
    }

    public static KeyDetails decodeKey(String key) {
        logger.debug("Key to decode: {}", key);
        if (key.startsWith("\"") && key.endsWith("\"")) {
            key = key.substring(1, key.length() - 1);
        }

        logger.debug("Cleaned key: {}", key);

        String[] split = key.split(":");
        String application = split[0];
        String variable = split[1];
        return new KeyDetails(variable, application);
    }

    public Sink<String, CompletionStage<IOResult>> lineSink(String filename) {
        return Flow.of(String.class)
                .map(s -> ByteString.fromString(s.toString() + "\n"))
                .toMat(FileIO.toPath(Paths.get(filename)), Keep.right());
    }
}
