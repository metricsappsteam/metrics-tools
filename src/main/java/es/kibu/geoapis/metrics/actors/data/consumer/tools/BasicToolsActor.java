package es.kibu.geoapis.metrics.actors.data.consumer.tools;

import akka.actor.Cancellable;
import akka.actor.Props;
import es.kibu.geoapis.backend.actors.tools.ToolsActor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import scala.concurrent.duration.FiniteDuration;

import java.util.concurrent.TimeUnit;

/**
 * Created by lrodriguez2002cu on 03/03/2017.
 */
public class BasicToolsActor extends ToolsActor {

    @Override
    public boolean getDoHeartbeat() {
        return true;
    }

    Logger logger = LoggerFactory.getLogger(BasicToolsActor.class);

    public static Props props(){
        return Props.create(BasicToolsActor.class);
    }

    @Override
    public void onReceive(Object message) throws Throwable {
       logger.debug("BasicToolsActor is running...");
       if (message instanceof String){
           if (((String) message).equalsIgnoreCase("tick")) {
               logger.debug("tick happened");
           }
       }
    }

    Cancellable tick;

    @Override
    public void preStart() throws Exception {
        super.preStart();

        Cancellable tick = getContext().system().scheduler().schedule(new FiniteDuration(500, TimeUnit.MILLISECONDS), new FiniteDuration(1000, TimeUnit.MILLISECONDS),
                getSelf(), "tick", getContext().system().dispatcher(), getSelf());
    }

    @Override
    public void postStop() throws Exception {
        super.postStop();
        tick.cancel();
    }


}
