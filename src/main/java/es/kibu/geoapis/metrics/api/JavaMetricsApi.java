package es.kibu.geoapis.metrics.api;

import com.google.gson.Gson;
import es.kibu.geoapis.api.Configurable;
import es.kibu.geoapis.api.DataSubmitter;
import es.kibu.geoapis.api.MetricsApi;
import es.kibu.geoapis.api.Tracker;
import es.kibu.geoapis.data.DataContext;
import es.kibu.geoapis.data.providers.RandomUserAndSessionProvider;
import es.kibu.geoapis.data.providers.components.DaggerWiredTrackerFactory;
import es.kibu.geoapis.data.providers.components.WiredTrackerFactory;
import es.kibu.geoapis.data.providers.modules.DefaultSubmitterModule;
import es.kibu.geoapis.data.providers.modules.RandomProvidersModule;
import es.kibu.geoapis.data.providers.modules.TrackerModule;
import es.kibu.geoapis.metrics.MetricsCoreUtils;
import es.kibu.geoapis.objectmodel.MetricsDefinition;
import es.kibu.geoapis.serialization.GsonUtils;
import es.kibu.geoapis.services.objectmodel.results.MetricsResult;
import es.kibu.geoapis.services.objectmodel.results.ResultValue;
import es.kibu.geoapis.services.objectmodel.results.ResultValueReader;
import es.kibu.geoapis.services.objectmodel.results.ServicesUtils;
import okhttp3.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;


/**
 * Created by lrodriguez2002cu on 03/03/2017.
 */
public class JavaMetricsApi extends MetricsApi {

    public static String BASE_URL = "";//"http://geobd01.init.uji.es:9999";

    static Logger logger = LoggerFactory.getLogger(JavaMetricsApi.class);
    private JavaDataSubmitter dataSubmitter;


    public static void init(Configurable config, String baseURL) {

        JavaMetricsApi.BASE_URL = baseURL;

        logger.debug(String.format("Initializing Java Metrics API {URL: %s, app: %s, session: %s, user: %s}",
                baseURL, config.getApplication(), config.getSession(), config.getUser()));

        api = new JavaMetricsApi(config);
        logger.debug("Metrics API initialized successfully");

    }

    public JavaMetricsApi(Configurable config) {
        super(config);
        dataSubmitter = new JavaDataSubmitter(getConfig());
    }

    @Override
    public MetricsDefinition getMetricsDefinition() {
        return dataSubmitter.getMetricsDefinition();
    }

    @Override
    protected Tracker getTracker(boolean forTesting) {
        logger.debug("Creating tracker Java testing: {} ", forTesting);
        WiredTrackerFactory factory = DaggerWiredTrackerFactory.builder()
                .randomProvidersModule(new RandomProvidersModule(new RandomUserAndSessionProvider(500), config))
                .trackerModule(new TrackerModule())
                .defaultSubmitterModule(new DefaultSubmitterModule(dataSubmitter)).build();

        return factory.createTracker();
    }

    public static class JavaDataSubmitter implements DataSubmitter {

        public static final MediaType JSON
                = MediaType.parse("application/json; charset=utf-8");

        OkHttpClient client = new OkHttpClient();

        Logger logger = LoggerFactory.getLogger(JavaDataSubmitter.class);

        Gson gson = GsonUtils.getGson();

        Configurable config;

        public JavaDataSubmitter(Configurable config) {
            this.config = config;
        }

        String post(String url, String json) throws IOException {
            RequestBody body = RequestBody.create(JSON, json);
            Request request = new Request.Builder()
                    .url(url)
                    .post(body)
                    .build();
            Response response = client.newCall(request).execute();

            return response.body().string();
        }

        String get(String url) throws IOException {
            Request request = new Request.Builder()
                    .url(url)
                    .build();
            Response response = client.newCall(request).execute();
            return response.body().string();
        }

        static class JavaMetricsSubmitterException extends RuntimeException {

            public JavaMetricsSubmitterException(String message) {
                super(message);
            }

            public JavaMetricsSubmitterException(Throwable cause) {
                super(cause);
            }

            public JavaMetricsSubmitterException(String message, Throwable cause) {
                super(message, cause);
            }
        }

        public MetricsDefinition getMetricsDefinition() {
            ServicesUtils.withBaseUrl(BASE_URL);
            String submitDataUrl = ServicesUtils.getMetricsUrl(config.getApplication());
            try {
                String result = get(submitDataUrl);
                ResultValue<MetricsResult> metricsResultResultValue = ResultValueReader.asMetricResult(result);
                if (!metricsResultResultValue.isError()) {
                    return MetricsCoreUtils.withMetricDefinitionFromContent(metricsResultResultValue.getResult().getContent());
                } else
                    throw new JavaMetricsSubmitterException(String.format("Error while retriving metrics definition: %s",
                            metricsResultResultValue.getErrorMessage()));
            } catch (Exception e) {
                logger.error("Error occurred while retrieving definition {}", e);
                throw new JavaMetricsSubmitterException(e);
            }
        }

        @Override
        public void submit(Object data, DataContext context) {
            logger.debug("Submitting data..");
            ServicesUtils.withBaseUrl(BASE_URL);
            String submitDataUrl = ServicesUtils.getSubmitDataUrl(config.getApplication(), context.getVariable().getName());
            try {
                String body = gson.toJson(data);
                logger.debug("Data to be submitted: {}", body);
                String responseBody = post(submitDataUrl, body);
                logger.debug("response: {}", responseBody);
            } catch (IOException e) {
                logger.error("Error submitting data", e);

            }
            logger.debug("Data submitted...");
        }
    }

}
