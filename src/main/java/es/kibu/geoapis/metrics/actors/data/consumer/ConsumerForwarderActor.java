package es.kibu.geoapis.metrics.actors.data.consumer;

import akka.actor.ActorRef;
import akka.actor.Props;
import akka.actor.UntypedActor;
import es.kibu.geoapis.metrics.actors.data.WriteInstruction;
import es.kibu.geoapis.metrics.actors.data.writer.DataWriterActor;
import es.kibu.geoapis.metrics.config.ConfigUtils;
import es.kibu.geoapis.metrics.config.SettingsImpl;
import es.kibu.geoapis.metrics.engine.data.writing.cassandra.CassandraConfigurator;
import es.kibu.geoapis.metrics.engine.data.writing.cassandra.CassandraSettings;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import scala.reflect.ClassTag;

import java.io.Serializable;

/**
 * Created by lrodriguez2002cu on 30/12/2016.
 */
public class ConsumerForwarderActor extends UntypedActor {

    Logger logger = LoggerFactory.getLogger(ConsumerForwarderActor.class);

    public static Props props() {
        ClassTag<ConsumerForwarderActor> tag = scala.reflect.ClassTag$.MODULE$.apply(ConsumerForwarderActor.class);
        return Props.apply(tag);
    }

    public static class DefaultCassandraConfigurator implements CassandraConfigurator {

        @Override
        public CassandraSettings getSettings(Object settings) {
            if (settings instanceof SettingsImpl) {
                return fromSettingsImpl((SettingsImpl) settings);
            } else if (settings == null) {
                SettingsImpl settings1 = ConfigUtils.getSettings();
                return fromSettingsImpl(settings1);
            }
            return null;
        }

        public static  CassandraSettings getCassandraSettings() {
            DefaultCassandraConfigurator defaultCassandraConfigurator = new DefaultCassandraConfigurator();
            return defaultCassandraConfigurator.getSettings(null);
        }

        public static CassandraSettings fromSettingsImpl(SettingsImpl settingsProvider) {

            return new CassandraSettings() {

                @Override
                public String getCassandraDBUser() {
                    return settingsProvider.CASSANDRA_DB_USER;
                }

                @Override
                public String getCassandraDBPass() {
                    return settingsProvider.CASSANDRA_DB_PASS;
                }

                @Override
                public String getCassandraDBName() {
                    return settingsProvider.CASSANDRA_DB_NAME;
                }

                @Override
                public String getCassandraDBUri() {
                    return settingsProvider.CASSANDRA_DB_URI;
                }

                @Override
                public void setCassandraDBName(String cassandraDBName) {

                }

                @Override
                public void setCassandraDBUri(String cassandraDBUri) {

                }
            };
        }
    }


    ActorRef writerRef;

    private ActorRef getWriterActorRef() {
        //todo: change this!! This might be even better resolved by a  router or other less coupled mechanism
        if (writerRef == null) {
            CassandraSettings settings = DefaultCassandraConfigurator.getCassandraSettings();
            boolean runFake = ConfigUtils.getConfig().getBoolean("dry-run");
            writerRef = this.context().actorOf(DataWriterActor.props(settings, runFake));
        }
        return writerRef;
    }


    public static class CompleteObject implements Serializable {

    }

    @Override
    public void onReceive(Object message) throws Throwable {
        if (message instanceof CompleteObject) {
            logger.debug("Complete message received", message);
        } else if (message instanceof WriteInstruction) {
            logger.debug("Forwarding message: {} ", message);
            getWriterActorRef().forward(message, this.context());
        }
    }
}
