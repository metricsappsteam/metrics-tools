package es.kibu.geoapis.metrics.config;

import com.typesafe.config.Config;
import com.typesafe.config.ConfigFactory;
import com.typesafe.config.ConfigValue;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Map;


/**
 * Created by lrodriguez2002cu on 27/10/2016.
 */
public class ConfigUtils {



    //TODO: unify this with the file in metrics engine
    public static final String METRICS_BACKEND_PRINT_CONFIG_AT_STARTUP = "metrics-backend.print-config-at-startup";
    static Logger logger = LoggerFactory.getLogger(ConfigUtils.class);

    public static final String DATA_INGESTION_CONF = "metrics-tools.conf";

    public static Config getConfig() {
        Config config = ConfigFactory.load(DATA_INGESTION_CONF);
        return config;
    }

    public static SettingsImpl getSettings() {
        return new SettingsImpl(getConfig());
    }

    public static void prettyPrintConfig() {

        if (getConfig().hasPath(METRICS_BACKEND_PRINT_CONFIG_AT_STARTUP) && getConfig().getBoolean(METRICS_BACKEND_PRINT_CONFIG_AT_STARTUP)) {
            for (Map.Entry<String, ConfigValue> stringConfigValueEntry : getConfig().entrySet()) {
                logger.debug("{}:{}", stringConfigValueEntry.getKey(), stringConfigValueEntry.getValue().render());
            }
        }
    }
}
