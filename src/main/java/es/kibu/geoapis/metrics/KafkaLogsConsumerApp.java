package es.kibu.geoapis.metrics;

import akka.actor.ActorSystem;
import com.typesafe.config.Config;
import es.kibu.geoapis.metrics.actors.data.consumer.KafkaConsumer;
import es.kibu.geoapis.metrics.actors.data.consumer.KafkaLogsConsumer;
import es.kibu.geoapis.metrics.actors.data.consumer.SinkFactory;
import es.kibu.geoapis.metrics.config.ConfigUtils;
import kamon.Kamon;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Created by lrodriguez2002cu on 14/02/2017.
 */
public class KafkaLogsConsumerApp {

    static Logger logger = LoggerFactory.getLogger(KafkaLogsConsumerApp.class);

    public static void main(String[] args) {

        Kamon.start();
        Config config = ConfigUtils.getConfig();
        ActorSystem system = ActorSystem.create("KafkaConsumerSystem", config);

        logger.debug("/-------------------------------------------");
        logger.debug(String.format("| Starting Data Engine.."));
        logger.debug("\\------------------------------------------");

        logger.debug(String.format("Starting Kamon.."));
        logger.debug(String.format("Kamon started"));

        ConfigUtils.prettyPrintConfig();

        KafkaLogsConsumer consumer = KafkaLogsConsumer.withLoggerSink(system, config.getConfig("akka.kafka.consumer"));
        //consumer.start(system, ConfigUtils.DATA_INGESTION_CONF, SinkFactory.cassandraSinkActor(system));
    }
}
